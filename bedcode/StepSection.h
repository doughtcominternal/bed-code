class StepSection
{
public:
    long NumberOfSteps;
    float Speed;
    
    StepSection()
    {

    }
    
    StepSection(long sectionSteps, long sectionSpeed)
    {
        NumberOfSteps = sectionSteps;
        Speed = sectionSpeed;
    }
};