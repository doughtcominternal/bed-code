#define BedButton_h

#include "Arduino.h"

enum ButtonState {
    Depressed,
    Pressed,
    LongPressed
};

class BedButton 
{
private:
    int _lastInputState = HIGH;
    long _startPress = 0;
    long _endPress = 0;
    int _buttonPin;
    int _idlePressBuffer = 300;
    int _longPressBuffer = 1700;
    ButtonState _lastReportedState = Depressed;
public:
    BedButton(int pinNumber, int idlePressBuffer = 300, int longPressBuffer = 2000);
    ButtonState GetButtonState();
    ButtonState GetCurrentButtonState();
};