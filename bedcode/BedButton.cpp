#include "BedButton.h"

BedButton::BedButton(int pinNumber = 15, int idlePressBuffer = 300, int longPressBuffer = 2000)
{
    _buttonPin = pinNumber;
    pinMode(_buttonPin, INPUT_PULLUP);
    _idlePressBuffer = idlePressBuffer;
    _longPressBuffer = longPressBuffer - idlePressBuffer;
}

ButtonState BedButton::GetButtonState()
{    
    int inputState = digitalRead(_buttonPin);

    if (inputState == LOW && _lastInputState == HIGH && millis() - _endPress >= _idlePressBuffer && _lastReportedState != Pressed)
    {
        _startPress = millis();
        
        _lastReportedState = Pressed;
        _lastInputState = LOW;
        return Pressed;
    }
    else if (inputState == LOW && _lastInputState == LOW && _startPress > 0 && millis() - _startPress >= _longPressBuffer && _lastReportedState != LongPressed)
    {
        _lastReportedState = LongPressed;
        return LongPressed;
    }
    else if (inputState == HIGH && _lastInputState == LOW && _lastReportedState != Depressed)
    {
        _lastInputState = HIGH;

        _startPress = 0;
        _endPress = millis();
        _lastReportedState = Depressed;
        return Depressed;
    }

    return Depressed;
}

//Used for getting the current button state no matter what other rules/debouncers are in place.
//Note: used for checking the state when pressing both buttons to shut off relays
ButtonState BedButton::GetCurrentButtonState()
{
    int inputState = digitalRead(_buttonPin);

    if (inputState == LOW && millis() - _startPress < _longPressBuffer)
    {
        return Pressed;
    }
    else if (inputState == LOW && _startPress > 0 && millis() - _startPress >= _longPressBuffer)
    {
        return LongPressed;
    }
    else if (inputState == HIGH)
    {
        return Depressed;
    }

    return Depressed;
}