//Connecting to a Pro Mini
//https://alselectro.wordpress.com/2017/05/14/arduino-pro-mini-how-to-upload-code/
//Pro Mini Pinout
//https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/46312c8c-6248-4962-9708-c60a7f579363/d96h91k-5cc4a516-b421-449a-9ff6-16142f974b79.png/v1/fill/w_1024,h_725,strp/_arduino_r___like_pro_mini_pinout_diagram_by_adlerweb_d96h91k-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD03MjUiLCJwYXRoIjoiXC9mXC80NjMxMmM4Yy02MjQ4LTQ5NjItOTcwOC1jNjBhN2Y1NzkzNjNcL2Q5Nmg5MWstNWNjNGE1MTYtYjQyMS00NDlhLTlmZjYtMTYxNDJmOTc0Yjc5LnBuZyIsIndpZHRoIjoiPD0xMDI0In1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.I2d6gMIPbd5NUq4oUUP9m_ua2X9Z5sTYYUmnRwpMvmE
//example: P2000 400 - 2000 steps (5 revolution with 400 step/rev microstepping) and 400 steps/s speed = 5 seconds

//Todo: Add Side Actuator Code : Done?
//Todo: Add Linear Actuator Code : Done?
//Todo: Add code to hold both buttons to shutoff relays after the bed is secured : Done
//Todo: Figure out what's causing the weird speed issue now :( : Done?
//Todo: Add code for the limiter switches : Done?
//Todo: Cleanup whatever is causing the delay between setting the speeds : Done?
//Todo: Add code for relay shutoff (when holding both buttons) to check relays : Done


#include <AccelStepper.h>
#include "StepConfig.h"
#include "BedButton.h"

enum Command
{
    None,
    Up,
    Down
};

enum SystemState {
    Idle,
    GoingUp,
    GoingDown,
    PausedUp,
    PausedDown,
};

//User-defined values
char receivedCommand;

//-------------------------------------------------------------------------------
//PROGMEM Constants
const char asciiArt1[] PROGMEM = ".-----.----.-----.--.--.    .--|  |.--.--.----.|  |--.    .-----.-----.";
const char asciiArt2[] PROGMEM = "|  _  |   _|  -__|  |  |    |  _  ||  |  |  __||    <     |  _  |__ --|";
const char asciiArt3[] PROGMEM = "|___  |__| |_____|___  |    |_____||_____|____||__|__|    |_____|_____|";
const char asciiArt4[] PROGMEM = "|_____|          |_____|            grey duck bed Operating System V1.0";
PGM_P const asciiTable[] PROGMEM { asciiArt1, asciiArt2, asciiArt3, asciiArt4 };

 

//Constants
const float LEVERAGE_MULTIPLIER = 3; // = 3; for normal operation (3:1 pulley), = .25 for testing

const int RELAY_STEPPERS_PIN = 2;

const int STEPPER_FRONT_BED_DIRECTION_PIN = 3;
const int STEPPER_FRONT_BED_PULSE_PIN = 4;
const int STEPPER_REAR_BED_DIRECTION_PIN = 5;
const int STEPPER_REAR_BED_PULSE_PIN = 6;
const int STEPPER_LINEAR_DIRECTION_PIN = 9;
const int STEPPER_LINEAR_PULSE_PIN = 10;

const int SWITCH_FRONT_UP_PIN = 11;
const int SWITCH_FRONT_DOWN_PIN = 12;
const int SWITCH_REAR_UP_PIN = A3;
const int SWITCH_REAR_DOWN_PIN = A0;

const int BUTTON_UP_PIN = A1;
const int BUTTON_DOWN_PIN = A2;

const int PRESS_REQUIREMENT = 2700; //The amount of time that's required to press the button before something happens (XXXX - IDLE_PRESS_BUFFER)
const int IDLE_PRESS_BUFFER = 300;  //The amount of time we ignore phantom presses from the arduino

StepSection upSteps[] = {
        StepSection(17000, 4000), //2900
        StepSection(10000, 4000), //2200
        StepSection(10000, 4000), //1000
        StepSection(12850, 4000), //600
};

StepSection downSteps[] = {
        StepSection(12850, 2900), //1100
        StepSection(10000, 2900), //1500
        StepSection(10000, 2900), //2200
        StepSection(16800, 2900), //2900 
};

StepSection linearUpSteps[] = {
        StepSection(1000, 1000)
};

StepSection linearDownSteps[] = {
        StepSection(1000, 1000)
};


//-------------------------------------------------------------------------------
//Runtime Variables
StepConfig bedRearStepperConfig(upSteps, downSteps, STEPPER_REAR_BED_DIRECTION_PIN, STEPPER_REAR_BED_PULSE_PIN, "Front", 4);
StepConfig bedFrontStepperConfig(upSteps, downSteps, STEPPER_FRONT_BED_DIRECTION_PIN, STEPPER_FRONT_BED_PULSE_PIN, "Rear", 4);
StepConfig linearStepperConfig(linearUpSteps, linearDownSteps, STEPPER_LINEAR_DIRECTION_PIN, STEPPER_LINEAR_PULSE_PIN, "Linear", 1);

BedButton buttonUp(BUTTON_UP_PIN);
BedButton buttonDown(BUTTON_DOWN_PIN);

SystemState currentState = Idle;
SystemState lastState = Idle; //Probably replace this once we have the limiter switches wired up

int directionMultiplier = 1; // = 1: positive direction, = -1: negative direction
int lastUpButtonState = HIGH, lastDownButtonState = HIGH; //Last states of each input button
long startPress = 0, endPress = 0;                         //Storage for time difference of when we're pressing buttons
bool testMode = false; //Used for testing inputs
bool runningCommand = false; //If we're running a serial command
bool disableActuators = false; //if we should move the steppers without moving the linear actuators
long maxBedSteps = 0; //the max steps the bed can/should move.

void StopBed(bool stopFrontSteppers = true, bool stopRearSteppers = true, bool stopLinearSteppers = true);

void setup()
{
    pinMode(RELAY_STEPPERS_PIN, OUTPUT);
    digitalWrite(RELAY_STEPPERS_PIN, HIGH);

    pinMode(SWITCH_FRONT_UP_PIN, INPUT_PULLUP);
    pinMode(SWITCH_FRONT_DOWN_PIN, INPUT_PULLUP);
    pinMode(SWITCH_REAR_UP_PIN, INPUT_PULLUP);
    pinMode(SWITCH_REAR_DOWN_PIN, INPUT_PULLUP);

    //Setup baud rate for serial communication for manual bed movement
    Serial.begin(9600);

    //Go ahead and get the max steps from one side of the bed passing in the multiplier constant so we're not calculating all the time
    //maxBedSteps = bedFrontStepperConfig.GetMaxSteps(LEVERAGE_MULTIPLIER);

    String output = "Leverage Multiplier Set to: ";
    
    char buffer[72];

    for (int i = 0; i < 4; i++)
    {
        strcpy_P(buffer, (PGM_P)pgm_read_word(&asciiTable[i]));

        Serial.println(buffer);
    }
    
    Serial.println("-----------------------------------------------------");
    Serial.println("Send 'C' for printing the commands.");
    Serial.println(output + LEVERAGE_MULTIPLIER);
 
    //setting up some default values for maximum speed and maximum acceleration
    bedFrontStepperConfig.Stepper.setAcceleration(4000); //ACCELERATION = Steps /(second)^2
    bedRearStepperConfig.Stepper.setAcceleration(4000); //ACCELERATION = Steps /(second)^2
    linearStepperConfig.Stepper.setAcceleration(4000); //ACCELERATION = Steps /(second)^2

    bedFrontStepperConfig.Stepper.disableOutputs(); //disable outputs
    bedRearStepperConfig.Stepper.disableOutputs(); //disable outputs
    linearStepperConfig.Stepper.disableOutputs(); //disable outputs
}

void loop()
{ 
    CheckSerial(); //check serial port for new commands
    RunTheBedMotors(); //function to handle the motor
    CheckButtonStates();
    CheckSwitchStates();
}

//Checks the limiter switch statements on each loop
void CheckSwitchStates()
{
    //If it doesn't match any of this carry on without going into the more complicated ifs (I think this is most efficient on each loop?)
    if (digitalRead(SWITCH_FRONT_DOWN_PIN) == LOW 
        || digitalRead(SWITCH_FRONT_UP_PIN) == LOW
        || digitalRead(SWITCH_REAR_DOWN_PIN) == LOW
        || digitalRead(SWITCH_REAR_UP_PIN) == LOW)
    {
        //If we're both limited on the down then we need to stop the bed
        if (digitalRead(SWITCH_FRONT_DOWN_PIN) == LOW && digitalRead(SWITCH_REAR_DOWN_PIN) == LOW && directionMultiplier < 0)
        {
            StopBed();
        }
        //If we're both limited on the up then we need to stop the bed
        else if (digitalRead(SWITCH_FRONT_UP_PIN) == LOW && digitalRead(SWITCH_REAR_UP_PIN) == LOW && directionMultiplier > 0)
        {
            StopBed();
        }
        //else we need to limit one side or the other and not stop the bed entirely
        else 
        {
            //If we're going down and either are hit, stop that side
            if ((digitalRead(SWITCH_FRONT_DOWN_PIN) == LOW || digitalRead(SWITCH_REAR_DOWN_PIN) == LOW) && directionMultiplier < 0)
            {
                if (digitalRead(SWITCH_FRONT_DOWN_PIN) == LOW)
                {
                    StopBed(true);
                }

                if (digitalRead(SWITCH_REAR_DOWN_PIN) == LOW)
                {
                    StopBed(false, true);
                }
            }
            //If we're going up and either are hit, stop that side
            if ((digitalRead(SWITCH_FRONT_UP_PIN) == LOW || digitalRead(SWITCH_REAR_UP_PIN) == LOW) && directionMultiplier > 0)
            {
                if (digitalRead(SWITCH_FRONT_UP_PIN) == LOW)
                {
                    StopBed(true);
                }

                if (digitalRead(SWITCH_REAR_UP_PIN) == LOW)
                {
                    StopBed(false, true);
                }
            }
        }
    }
}

//Checks the button states on each loop
void CheckButtonStates()
{
    ButtonState upButtonState = buttonUp.GetButtonState();
    ButtonState downButtonState = buttonDown.GetButtonState();
    ButtonState upButtonCurrentState = buttonUp.GetCurrentButtonState();
    ButtonState downButtonCurrentState = buttonDown.GetCurrentButtonState();

    //If either of these pass, then that means we just pressed the button and it's past the idle press buffer
    if (upButtonState == Pressed)
    {
        if (currentState == Idle && lastState == Idle)
        {
            Serial.println("UP Button Pressed");
            //Then we pause the bed
        }
        else if (currentState != Idle)
        {
            Serial.println("UP Button Pause");
            currentState = Idle;
            //Then we unpause the bed
        }
        else if (currentState == Idle && lastState != Idle)
        {
            Serial.println("UP Button UnPause");
            currentState = lastState;
        }
        lastUpButtonState = LOW;
    }
    else if (downButtonState == Pressed)
    {
        if (currentState == Idle && lastState == Idle)
        {
            Serial.println("DOWN Button Pressed");
            startPress = millis();
            //Then we pause the bed
        }
        else if (currentState != Idle)
        {
            Serial.println("DOWN Button Pause");
            currentState = Idle;
            //Then we unpause the bed
        }
        else if (currentState == Idle && lastState != Idle)
        {
            Serial.println("DOWN Button UnPause");
            currentState = lastState;
        }
        lastDownButtonState = LOW;
    }
    //If this passes that means we're holding both buttons in it's up position to turn the relay off.
    else if (upButtonCurrentState == LongPressed && downButtonCurrentState == LongPressed && currentState == Idle)
    {
        Serial.println("Shutting down bed");
        digitalWrite(RELAY_STEPPERS_PIN, HIGH);
    }
    //If either of these pass, then we're still holding the button
    else if (upButtonState == LongPressed && downButtonCurrentState != Pressed && currentState == Idle)
    {
        Serial.println("UP Button Pressed Required");
        MoveBedUp();
    }
    else if (downButtonState == LongPressed && upButtonCurrentState != Pressed && currentState == Idle)
    {
        Serial.println("DOWN Button Pressed Required");
        MoveBedDown();
    }
}

void RunTheBedConfig(StepConfig &stepConfig)
{
    //If we're going to a position via a command directly, then we don't go through the speeds/etc.
    if (runningCommand)
    {
        return;
    }

    long currentPosition = stepConfig.Stepper.currentPosition();

    //Serial.println(stepConfig.Name); //print the action
    //Serial.println(stepConfig.Stepper.maxSpeed());

    //If we're going up, this isn't a direct command and we haven't gone through all the speed indexes
    if (currentPosition >= stepConfig.CurrentLimit 
        && stepConfig.CurrentIndex + 1 < stepConfig.ArraySize 
        && currentState == GoingUp)
    {
        Serial.println("New Stepper Index Positive"); //print the action

        stepConfig.CurrentIndex++;

        stepConfig.CurrentLimit += stepConfig.UpSections[stepConfig.CurrentIndex].NumberOfSteps * LEVERAGE_MULTIPLIER;
        
        if (testMode)
        {
          
            Serial.println("Position");
            Serial.println(currentPosition);
            Serial.println("Limit");
            Serial.println(stepConfig.CurrentLimit);
            Serial.println("Index");
            Serial.println(stepConfig.CurrentIndex);
            Serial.println("Speed");
            Serial.println(stepConfig.UpSections[stepConfig.CurrentIndex].Speed);
        }

        //Set this section's speed
        stepConfig.Stepper.setMaxSpeed(stepConfig.UpSections[stepConfig.CurrentIndex].Speed);

        if (testMode)
        {
//            Serial.println("Set Speed");
//            Serial.println(stepConfig->Stepper.speed());   
        }
    }
    //If we're going down, this isn't a direct command and we haven't gone through all of the speed indexes
    else if (currentPosition <= stepConfig.CurrentLimit 
        && stepConfig.CurrentIndex + 1 < stepConfig.ArraySize 
        && currentState == GoingDown)
    {
        Serial.println("New Stepper Index Negative");

        stepConfig.CurrentIndex++;

        stepConfig.CurrentLimit -= stepConfig.DownSections[stepConfig.CurrentIndex].NumberOfSteps * LEVERAGE_MULTIPLIER;

        if (testMode)
        {
            Serial.println("Position");
            Serial.println(currentPosition);
            Serial.println("Limit");
            Serial.println(stepConfig.CurrentLimit);
            Serial.println("Index");
            Serial.println(stepConfig.CurrentIndex);
            Serial.println("Speed");
            Serial.println(stepConfig.DownSections[stepConfig.CurrentIndex].Speed);
        }

        //Set this section's speed
        stepConfig.Stepper.setMaxSpeed(stepConfig.DownSections[stepConfig.CurrentIndex].Speed);

        if (testMode)
        {
           Serial.println("Set Speed");
           Serial.println(stepConfig.Stepper.speed());
        }
    }
    //Then let's finish the task and keep the steppers active so the bed doesn't fall.
    else if (stepConfig.CurrentIndex + 1 == stepConfig.ArraySize 
        && (stepConfig.Name == "Rear" || stepConfig.Name == "Front")
        && currentPosition == maxBedSteps
        && currentState == GoingUp) 
    {
        Serial.println("Finished Up Position");
        lastState = Idle;
        currentState = Idle;
        stepConfig.CurrentIndex = 0;
    } 
    //Then let's finish our task and shut down everything since we were on our way down
    else if (stepConfig.CurrentIndex + 1 == stepConfig.ArraySize 
        && (currentPosition == 0 || currentPosition == stepConfig.CurrentLimit) 
        && (stepConfig.Name == "Rear" || stepConfig.Name == "Front")
        && currentState == GoingDown) 
    {
        Serial.println("Finished Down Position");
        Serial.println(currentPosition);
        lastState = Idle;
        currentState = Idle;
        stepConfig.CurrentIndex = 0;
        digitalWrite(RELAY_STEPPERS_PIN, HIGH);
    }
    else if (stepConfig.CurrentIndex + 1 == stepConfig.ArraySize 
        && stepConfig.Name == "Linear"
        && currentPosition == stepConfig.CurrentLimit
        && currentState == GoingUp) 
    {
        Serial.println("Finished Linear Up Position");
    }
    else if (stepConfig.CurrentIndex + 1 == stepConfig.ArraySize 
        && (currentPosition == 0 || currentPosition == stepConfig.CurrentLimit)
        && stepConfig.Name == "Linear"
        && currentState == GoingDown) 
    {
        Serial.println("Finished Linear Down Position");
    }
}

void MoveBedUp()
{
    digitalWrite(RELAY_STEPPERS_PIN, LOW);

    disableActuators = false;

    directionMultiplier = 1; //We define the direction
    currentState = GoingUp;
    lastState = GoingUp;
    Serial.println("Bed Going Up."); //print the action

    bedRearStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    bedRearStepperConfig.Stepper.setCurrentPosition(0); //Reset the position to 0
    bedRearStepperConfig.CurrentLimit = 0;
    bedFrontStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    bedFrontStepperConfig.Stepper.setCurrentPosition(0); //Reset the position to 0
    bedFrontStepperConfig.CurrentLimit = 0;
    linearStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    linearStepperConfig.Stepper.setCurrentPosition(0); //Reset the position to 0
    linearStepperConfig.CurrentLimit = 0;
    RunTheBedMotors();

    Rotate(&bedRearStepperConfig); //Run the function
    Rotate(&bedFrontStepperConfig); //Run the function
    Rotate(&linearStepperConfig); //Run the function
}

void MoveBedDown()
{
    digitalWrite(RELAY_STEPPERS_PIN, LOW);

    disableActuators = false;
    
    directionMultiplier = -1; //We define the direction
    currentState = GoingDown;
    lastState = GoingDown;
    Serial.println("Bed Going Down."); //print the action
    bedRearStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    bedRearStepperConfig.Stepper.setCurrentPosition(maxBedSteps); //Reset the position to 0
    bedRearStepperConfig.CurrentLimit = maxBedSteps;
    bedFrontStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    bedFrontStepperConfig.Stepper.setCurrentPosition(maxBedSteps); //Reset the position to 0
    bedFrontStepperConfig.CurrentLimit = maxBedSteps;
    linearStepperConfig.CurrentIndex = -1; //Set the index to -1 so it gets set to 0 when entering RunTheBedMotors()
    linearStepperConfig.Stepper.setCurrentPosition(0); //Reset the position to 0
    linearStepperConfig.CurrentLimit = 0;

    Serial.println(bedRearStepperConfig.Stepper.currentPosition()); //print the action
    Serial.println(bedRearStepperConfig.CurrentLimit); //print the action
    Serial.println(maxBedSteps);

    RunTheBedMotors();

    Serial.println(bedRearStepperConfig.Stepper.currentPosition()); //print the action
    Serial.println(bedRearStepperConfig.CurrentLimit); //print the action

    Rotate(&bedRearStepperConfig); //Run the function
    Rotate(&bedFrontStepperConfig); //Run the function
    Rotate(&linearStepperConfig); //Run the function
}

void RunTheBedMotors() //function for the motor
{
    //If we're not paused or idle then move the motor
    if (currentState == GoingUp || currentState == GoingDown)
    {
        bedFrontStepperConfig.Stepper.enableOutputs(); //enable pins
        bedFrontStepperConfig.Stepper.run(); //step the motor (this will step the motor by 1 step at each loop)  
        bedRearStepperConfig.Stepper.enableOutputs(); //enable pins
        bedRearStepperConfig.Stepper.run(); //step the motor (this will step the motor by 1 step at each loop)  
        linearStepperConfig.Stepper.enableOutputs(); //enable pins
        linearStepperConfig.Stepper.run(); //step the motor (this will step the motor by 1 step at each loop)  
    }
    //Otherwise don't spin the motors
    else 
    {
        bedFrontStepperConfig.Stepper.disableOutputs(); //disable outputs
        bedRearStepperConfig.Stepper.disableOutputs(); //disable outputs
        linearStepperConfig.Stepper.disableOutputs(); //disable outputs
        return;
    }

    RunTheBedConfig(bedFrontStepperConfig);
    RunTheBedConfig(bedRearStepperConfig);
    RunTheBedConfig(linearStepperConfig);
}

void RotateRelative(StepConfig *stepConfig, long steps, int speed)
{
    stepConfig->Stepper.move(steps * directionMultiplier);
}

void Rotate(StepConfig *stepConfig)
{
    digitalWrite(RELAY_STEPPERS_PIN, LOW);

    stepConfig->Stepper.move(stepConfig->GetMaxSteps(LEVERAGE_MULTIPLIER) * directionMultiplier); //set relative distance and direction
}

void CheckSerial() //function for receiving the commands
{  
    if (Serial.available() > 0) //if something comes from the computer
    {
        long receivedSteps = 0;
        long receivedSpeed = 0;

        receivedCommand = Serial.read(); // pass the value to the receivedCommad variable

        switch (receivedCommand) //we check what is the command
        {
        case 'U':
            MoveBedUp();

            break;

        case 'D':
            MoveBedDown();

            break;

        case 'P': //P uses the move() function of the AccelStepper library, which means that it moves relatively to the current position.              
            runningCommand = true;
            
            receivedSteps = Serial.parseFloat(); //value for the steps
            receivedSpeed = Serial.parseFloat(); //value for the speed
            
            directionMultiplier = 1; //We define the direction
            Serial.println("Positive direction."); //print the action

            currentState = GoingUp;
            //RotateRelative(&bedFrontStepperConfig, receivedSteps, receivedSpeed); //Run the function
            //RotateRelative(&bedRearStepperConfig, receivedSteps, receivedSpeed); //Run the function

            break;         

        case 'N': //N uses the move() function of the AccelStepper library, which means that it moves relatively to the current position.      
            runningCommand = true;

            receivedSteps = Serial.parseFloat(); //value for the steps
            receivedSpeed = Serial.parseFloat(); //value for the speed

            directionMultiplier = -1; //We define the direction
            Serial.println("Negative direction."); //print action

            currentState = GoingDown;
            //RotateRelative(&bedFrontStepperConfig, receivedSteps, receivedSpeed); //Run the function
            //RotateRelative(&bedRearStepperConfig, receivedSteps, receivedSpeed); //Run the function

            break;

        case 'S': // Stops the motor
            StopBed();
            
            break;

        case 'p': // Pauses the motor
            Serial.println("Paused."); //print action
            
            if (currentState == GoingDown)
            {
                currentState = PausedDown;
            } else {
                currentState = PausedUp;
            }

            //bedFrontStepperConfig.Stepper.stop(); //stop motor
            //bedRearStepperConfig.Stepper.stop(); //stop motor
            
            break;

        case 'r': // Runs the paused program
            Serial.println("Unpaused."); //print action

            if (currentState == PausedDown)
            {
                currentState = GoingDown;
            } else {
                currentState = GoingUp;
            }

            //bedFrontStepperConfig.Stepper.run(); //stop motor
            //bedRearStepperConfig.Stepper.run(); //stop motor

            break;

        case 'g': // Gets position
            //Serial.println(bedFrontStepperConfig.Stepper.currentPosition());
            
            break;

        case 'd': //disable actuator
            disableActuators = true;
            
            break;

        case 'e': //enable actuator
            disableActuators = false;
            
            break;

        case 'T':
            if (testMode)
            {
                testMode = false;
                Serial.println("Input Test Mode Deactivated.");
            } else {
                testMode = true;
                Serial.println("Input Test Mode Activated.");
            }
            
            break;

        case 't':


            break;

        default:  

            break;
        }    
    }
}

void StopBed(bool stopFrontSteppers, bool stopRearSteppers, bool stopLinearSteppers)
{   
    if (stopFrontSteppers)
    {
        bedFrontStepperConfig.Stepper.stop(); //stop motor
        bedFrontStepperConfig.Stepper.disableOutputs(); //disable power
    }

    if (stopRearSteppers)
    {
        bedRearStepperConfig.Stepper.stop(); //stop motor
        bedRearStepperConfig.Stepper.disableOutputs(); //disable power
    }

    if (stopLinearSteppers)
    {
        linearStepperConfig.Stepper.stop(); //stop motor
        linearStepperConfig.Stepper.disableOutputs(); //disable power
    }


    if (stopFrontSteppers && stopRearSteppers && stopLinearSteppers)
    {
        Serial.println("Stopped."); //print action
        currentState = Idle; //disable running
    }
}

void TestInputs()
{
    if (digitalRead(BUTTON_UP_PIN) == LOW)
    {
        Serial.println("Up Button Pressed");
    } 
    else if (digitalRead(BUTTON_DOWN_PIN) == LOW)
    {
        Serial.println("Down Button Pressed");
    }
    else if (digitalRead(SWITCH_FRONT_UP_PIN) == LOW)
    {
        Serial.println("Front Up Switch Pressed");
    }
    else if (digitalRead(SWITCH_FRONT_DOWN_PIN) == LOW)
    {
        Serial.println("Front Down Switch Pressed");
    }
    else if (digitalRead(SWITCH_REAR_DOWN_PIN) == LOW)
    {
        Serial.println("Back Down Switch Pressed");
    }
    else if (digitalRead(SWITCH_REAR_UP_PIN) == LOW)
    {
        Serial.println("Back Up Switch Pressed");
    }


    delay(2000);
}
 
void PrintCommands()
{  
    //Printing the commands
    Serial.println(" 'U' : Move the bed up, running the command sequences.");
    Serial.println(" 'D' : Move the bed down, running the command sequences.");
    Serial.println(" 'R' : Reset the bed after the pins are out since the bed shifted a bit.");
    Serial.println(" 'C' : Prints all the commands and their functions.");
    Serial.println(" 'P' : Rotates the motor in positive (CW) direction, relative.");
    Serial.println(" 'N' : Rotates the motor in negative (CCW) direction, relative.");
    Serial.println(" 'S' : Stops the motor immediately without the ability to resume."); 
    Serial.println(" 'A' : Sets an acceleration value."); 
    Serial.println(" 'u' : Updates the position the arduino keeps track of, of where the stepper motors are.");
    Serial.println(" 'g' : Gets the position the arduino keeps track of, of where the stepper motors are.");
    Serial.println(" 'e' : Enables linear actuators.");
    Serial.println(" 'd' : Disables linear actuators.");
    Serial.println(" 'p' : Pauses the up or down program in it's current position and stops everything immediately.");
    Serial.println(" 'r' : Resumes the up or down program in it's current position immediately.");
}
