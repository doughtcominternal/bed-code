#include "StepSection.h"

class StepConfig
{
public:
    long CurrentLimit;
    long CurrentIndex;
    AccelStepper Stepper;
    int DirectionPin;
    int PulsePin;
    StepSection *UpSections;
    StepSection *DownSections;
    char *Name;
    int ArraySize;
    
    StepConfig(StepSection *upSections, StepSection *downSections, int directionPin, int pulsePin, char *title, int arraySize)
    {
        this->UpSections = upSections;
        this->DownSections = downSections;
        this->Name = title;
        DirectionPin = directionPin;
        PulsePin = pulsePin;
        ArraySize = arraySize;
        Stepper = AccelStepper(1, directionPin, pulsePin);
    }

    long GetMaxSteps(float multiplier)
    {
        long maxSteps = 0;

        for (int i = 0; i < 4; i++)
        {
            maxSteps += UpSections[i].NumberOfSteps;
        };

        return maxSteps * multiplier;
    }
};
