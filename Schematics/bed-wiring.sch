EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DIN-5 GX16-5-1
U 1 1 600CA951
P 3000 1750
F 0 "GX16-5-1" H 3000 1475 50  0000 C CNN
F 1 "Stepper Front" H 3000 1384 50  0000 C CNN
F 2 "" H 3000 1750 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3000 1750 50  0001 C CNN
	1    3000 1750
	1    0    0    -1  
$EndComp
$Comp
L Connector:DIN-5 GX16-5-2
U 1 1 600CD284
P 3000 2650
F 0 "GX16-5-2" H 3000 2375 50  0000 C CNN
F 1 "Stepper Rear" H 3000 2284 50  0000 C CNN
F 2 "" H 3000 2650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3000 2650 50  0001 C CNN
	1    3000 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:DIN-5 GX16-5-3
U 1 1 600CE88A
P 3000 3550
F 0 "GX16-5-3" H 3000 3275 50  0000 C CNN
F 1 "Linear Stepper" H 3000 3184 50  0000 C CNN
F 2 "" H 3000 3550 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3000 3550 50  0001 C CNN
	1    3000 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:DIN-5 GX16-5-4
U 1 1 600CFCCA
P 3000 4450
F 0 "GX16-5-4" H 3000 4175 50  0000 C CNN
F 1 "Side Stepper" H 3000 4084 50  0000 C CNN
F 2 "" H 3000 4450 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3000 4450 50  0001 C CNN
	1    3000 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2_Ground AudioJack1
U 1 1 600D1632
P 4200 4500
F 0 "AudioJack1" H 4232 4825 50  0000 C CNN
F 1 "Up/Down Button" H 4232 4734 50  0000 C CNN
F 2 "" H 4200 4500 50  0001 C CNN
F 3 "~" H 4200 4500 50  0001 C CNN
	1    4200 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2_Ground AudioJack2
U 1 1 600D2F7E
P 5150 4500
F 0 "AudioJack2" H 5182 4825 50  0000 C CNN
F 1 "Front Limiter Switch" H 5182 4734 50  0000 C CNN
F 2 "" H 5150 4500 50  0001 C CNN
F 3 "~" H 5150 4500 50  0001 C CNN
	1    5150 4500
	1    0    0    -1  
$EndComp
$Comp
L Relay:FINDER-40.51 Relay1
U 1 1 600D50F5
P 7950 5000
F 0 "Relay1" H 8380 5046 50  0000 L CNN
F 1 "Bed Relay" H 8380 4955 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Finder_40.51" H 9090 4960 50  0001 C CNN
F 3 "http://gfinder.findernet.com/assets/Series/353/S40EN.pdf" H 7950 5000 50  0001 C CNN
	1    7950 5000
	1    0    0    -1  
$EndComp
$Comp
L Relay:FINDER-40.51 Relay2
U 1 1 600D6DCF
P 9650 5000
F 0 "Relay2" H 10080 5046 50  0000 L CNN
F 1 "Linear Relay" H 10080 4955 50  0000 L CNN
F 2 "Relay_THT:Relay_SPDT_Finder_40.51" H 10790 4960 50  0001 C CNN
F 3 "http://gfinder.findernet.com/assets/Series/353/S40EN.pdf" H 9650 5000 50  0001 C CNN
	1    9650 5000
	1    0    0    -1  
$EndComp
$Comp
L Connector:AudioJack2_Ground AudioJack3
U 1 1 600D95A5
P 6150 4500
F 0 "AudioJack3" H 6182 4825 50  0000 C CNN
F 1 "Rear Limiter Switch" H 6182 4734 50  0000 C CNN
F 2 "" H 6150 4500 50  0001 C CNN
F 3 "~" H 6150 4500 50  0001 C CNN
	1    6150 4500
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_UNO_R3 PrimaryController
U 1 1 600C6F19
P 8350 2150
F 0 "PrimaryController" H 8350 3331 50  0000 C CNN
F 1 "Arduino Mini" H 8350 3240 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 8350 2150 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 8350 2150 50  0001 C CNN
	1    8350 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5550 7750 5300
Wire Wire Line
	7750 5550 9450 5550
Wire Wire Line
	9450 5550 9450 5300
$Comp
L Regulator_Switching:GL2576-ATA5R BuckConverter
U 1 1 600F3002
P 5100 1150
F 0 "BuckConverter" H 5100 1517 50  0000 C CNN
F 1 "24V-5V Buck Converter" H 5100 1426 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-5_TabPin3" H 5100 900 50  0001 L CIN
F 3 "http://www.dianyuan.com/bbs/u/54/437861181916300.pdf" H 5100 1150 50  0001 C CNN
	1    5100 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5550 7200 5550
Wire Wire Line
	7200 5550 7200 3900
Wire Wire Line
	7200 3900 8450 3900
Wire Wire Line
	8450 3900 8450 3250
Connection ~ 7750 5550
Wire Wire Line
	9450 4700 9450 4350
Wire Wire Line
	9450 4350 7750 4350
Wire Wire Line
	7750 4350 7750 4700
Wire Wire Line
	7750 4350 7450 4350
Wire Wire Line
	7450 4350 7450 1750
Connection ~ 7750 4350
Wire Wire Line
	8250 4700 8250 4600
Wire Wire Line
	8250 4600 6750 4600
Wire Wire Line
	6750 4600 6750 5400
Wire Wire Line
	6750 5400 2450 5400
Wire Wire Line
	2450 5400 2450 4550
Wire Wire Line
	2450 4550 2700 4550
Wire Wire Line
	2450 4550 2450 3650
Wire Wire Line
	2450 3650 2700 3650
Connection ~ 2450 4550
Wire Wire Line
	9950 4700 9950 4500
Wire Wire Line
	9950 4500 6650 4500
Wire Wire Line
	6650 4500 6650 5300
Wire Wire Line
	6650 5300 2350 5300
Wire Wire Line
	2350 5300 2350 2750
Wire Wire Line
	2350 2750 2700 2750
Wire Wire Line
	2350 2750 2350 1850
Wire Wire Line
	2350 1850 2700 1850
Connection ~ 2350 2750
Wire Wire Line
	3000 4150 3650 4150
Wire Wire Line
	3650 4150 3650 3250
Wire Wire Line
	3650 3250 3000 3250
Wire Wire Line
	3650 3250 3650 2350
Wire Wire Line
	3650 2350 3000 2350
Connection ~ 3650 3250
Wire Wire Line
	3650 2350 3650 1450
Wire Wire Line
	3650 1450 3000 1450
Connection ~ 3650 2350
Wire Wire Line
	5100 2350 3650 2350
Wire Wire Line
	5100 1450 5100 1550
Wire Wire Line
	8350 3250 8350 3350
Wire Wire Line
	8350 3350 5700 3350
Wire Wire Line
	3750 3350 3750 1350
Wire Wire Line
	3750 1350 2550 1350
Wire Wire Line
	2550 1350 2550 1650
Wire Wire Line
	2550 1650 2700 1650
Wire Wire Line
	2700 2550 2550 2550
Wire Wire Line
	2550 2550 2550 1650
Connection ~ 2550 1650
Wire Wire Line
	2700 3450 2550 3450
Connection ~ 2550 2550
Wire Wire Line
	2700 4350 2550 4350
Wire Wire Line
	2550 2550 2550 3450
Connection ~ 2550 3450
Wire Wire Line
	2550 3450 2550 4350
Wire Wire Line
	6150 4700 6150 4850
Wire Wire Line
	6150 4850 5700 4850
Wire Wire Line
	5150 4850 5150 4700
Wire Wire Line
	5150 4850 4200 4850
Wire Wire Line
	4200 4850 4200 4700
Connection ~ 5150 4850
Wire Wire Line
	5700 4850 5700 3350
Connection ~ 5700 4850
Wire Wire Line
	5700 4850 5150 4850
Connection ~ 5700 3350
Wire Wire Line
	5700 3350 3750 3350
Wire Wire Line
	5600 1250 7200 1250
Wire Wire Line
	7200 1250 7200 800 
Wire Wire Line
	7200 800  8250 800 
Wire Wire Line
	8250 800  8250 1150
Wire Wire Line
	5600 1050 6600 1050
Wire Wire Line
	6600 1050 6600 3250
Wire Wire Line
	6600 3250 8250 3250
Wire Wire Line
	3300 1650 7350 1650
Wire Wire Line
	7350 1650 7350 1850
Wire Wire Line
	7350 1850 7850 1850
Wire Wire Line
	3300 1850 7250 1850
Wire Wire Line
	7250 1850 7250 1950
Wire Wire Line
	7250 1950 7850 1950
Wire Wire Line
	3300 2550 6500 2550
Wire Wire Line
	6500 2550 6500 2050
Wire Wire Line
	6500 2050 7850 2050
Wire Wire Line
	3300 2750 6400 2750
Wire Wire Line
	6400 2750 6400 2150
Wire Wire Line
	6400 2150 7850 2150
Wire Wire Line
	3300 3450 3850 3450
Wire Wire Line
	3850 3450 3850 2850
Wire Wire Line
	3850 2850 6300 2850
Wire Wire Line
	6300 2850 6300 2250
Wire Wire Line
	6300 2250 7850 2250
Wire Wire Line
	3300 3650 3950 3650
Wire Wire Line
	3950 3650 3950 2950
Wire Wire Line
	3950 2950 6200 2950
Wire Wire Line
	6200 2950 6200 2350
Wire Wire Line
	6200 2350 7850 2350
Wire Wire Line
	3300 4350 3750 4350
Wire Wire Line
	3750 4350 3750 3750
Wire Wire Line
	3750 3750 4050 3750
Wire Wire Line
	4050 3750 4050 3050
Wire Wire Line
	4050 3050 6100 3050
Wire Wire Line
	6100 3050 6100 2450
Wire Wire Line
	6100 2450 7850 2450
Wire Wire Line
	3300 4550 3850 4550
Wire Wire Line
	3850 4550 3850 3850
Wire Wire Line
	3850 3850 4150 3850
Wire Wire Line
	4150 3850 4150 3150
Wire Wire Line
	4150 3150 6000 3150
Wire Wire Line
	6000 3150 6000 2650
Wire Wire Line
	6000 2650 6700 2650
Wire Wire Line
	6700 2650 6700 2550
Wire Wire Line
	6700 2550 7850 2550
Wire Wire Line
	5350 4400 5600 4400
Wire Wire Line
	5600 4400 5600 3250
Wire Wire Line
	5600 3250 6200 3250
Wire Wire Line
	6200 3250 6200 3050
Wire Wire Line
	6200 3050 6850 3050
Wire Wire Line
	6850 3050 6850 2650
Wire Wire Line
	6850 2650 7850 2650
Wire Wire Line
	5350 4500 5500 4500
Wire Wire Line
	5500 4500 5500 3450
Wire Wire Line
	5500 3450 6950 3450
Wire Wire Line
	6950 3450 6950 2750
Wire Wire Line
	6950 2750 7850 2750
Wire Wire Line
	8850 2150 9300 2150
Wire Wire Line
	9300 2150 9300 3800
Wire Wire Line
	9300 3800 6450 3800
Wire Wire Line
	6450 3800 6450 4400
Wire Wire Line
	6450 4400 6350 4400
Wire Wire Line
	8550 3900 8550 4000
Wire Wire Line
	8550 4000 6550 4000
Wire Wire Line
	6550 4000 6550 4500
Wire Wire Line
	6550 4500 6350 4500
Wire Wire Line
	4400 4400 4550 4400
Wire Wire Line
	4550 4400 4550 3550
Wire Wire Line
	4550 3550 9200 3550
Wire Wire Line
	9200 3550 9200 2250
Wire Wire Line
	9200 2250 8850 2250
Wire Wire Line
	4400 4500 4650 4500
Wire Wire Line
	4650 4500 4650 3650
Wire Wire Line
	4650 3650 9100 3650
Wire Wire Line
	9100 3650 9100 2350
Wire Wire Line
	9100 2350 8850 2350
Wire Wire Line
	8550 3900 9000 3900
Wire Wire Line
	9000 3900 9000 2450
Wire Wire Line
	9000 2450 8850 2450
$Comp
L power:+24V #PWR?
U 1 1 60154DCF
P 1150 1050
F 0 "#PWR?" H 1150 900 50  0001 C CNN
F 1 "+24V" H 1165 1223 50  0000 C CNN
F 2 "" H 1150 1050 50  0001 C CNN
F 3 "" H 1150 1050 50  0001 C CNN
	1    1150 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60156377
P 1150 1300
F 0 "#PWR?" H 1150 1050 50  0001 C CNN
F 1 "GND" H 1155 1127 50  0000 C CNN
F 2 "" H 1150 1300 50  0001 C CNN
F 3 "" H 1150 1300 50  0001 C CNN
	1    1150 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1050 2200 1050
Wire Wire Line
	1150 1300 1150 1250
Wire Wire Line
	1150 1250 3850 1250
Wire Wire Line
	3850 1250 3850 1550
Wire Wire Line
	3850 1550 5100 1550
Connection ~ 5100 1550
Wire Wire Line
	5100 1550 5100 2350
Wire Wire Line
	2200 1050 2200 5500
Wire Wire Line
	2200 5500 7100 5500
Wire Wire Line
	7100 5500 7100 5450
Wire Wire Line
	7100 5450 8150 5450
Wire Wire Line
	8150 5450 8150 5300
Connection ~ 2200 1050
Wire Wire Line
	2200 1050 4600 1050
Wire Wire Line
	8150 5450 9850 5450
Wire Wire Line
	9850 5450 9850 5300
Connection ~ 8150 5450
Wire Wire Line
	7450 1750 7850 1750
NoConn ~ 8850 1950
NoConn ~ 8850 1750
NoConn ~ 8850 1550
NoConn ~ 8850 2550
NoConn ~ 8850 2650
NoConn ~ 8850 2850
NoConn ~ 8850 2950
NoConn ~ 7850 1650
NoConn ~ 7850 1550
NoConn ~ 7850 2850
NoConn ~ 8450 1150
$EndSCHEMATC
